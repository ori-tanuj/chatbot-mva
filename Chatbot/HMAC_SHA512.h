//
//  HMAC_SHA512.h
//  OriObjective
//
//  Created by Kumari Akanksha on 25/01/20.
//  Copyright © 2020 Kumari Akanksha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>

NS_ASSUME_NONNULL_BEGIN

@interface HMAC_SHA512 : NSObject
+ (NSString *) hashedValue :(NSString *) key andData: (NSString *) data ;

@end

NS_ASSUME_NONNULL_END
