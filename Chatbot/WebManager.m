//
//  WebManager.m
//  OriObjective
//
//  Created by Kumari Akanksha on 25/01/20.
//  Copyright © 2020 Kumari Akanksha. All rights reserved.
//

#import "WebManager.h"

@implementation WebManager

+(UINavigationController*) openWebViewWithPhoneNumber:(NSString*)phoneNumber withCircle: (NSString*)circle withSegment: (NSString*)segment withAppVersion: (NSString*)appVersion {
    NSBundle *bundleName = [NSBundle bundleForClass:[WebManager class]];
    OriWebViewController *webView = [[OriWebViewController alloc] initWithNibName:@"OriWebViewController" bundle:bundleName];
    NSString *url = [bundleName pathForResource:@"ori" ofType:@"html"];
    [webView initViewWithUrl:url withPhoneNumber:phoneNumber withCircle:circle withSegment:segment withAppVersion:appVersion];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:webView];
    return nav;
//    return webView;
}


@end
