//
//  HMAC_SHA512.m
//  OriObjective
//
//  Created by Kumari Akanksha on 25/01/20.
//  Copyright © 2020 Kumari Akanksha. All rights reserved.
//

#import "HMAC_SHA512.h"
#import <CommonCrypto/CommonHMAC.h>

@implementation HMAC_SHA512

+ (NSString *) hashedValue :(NSString *) key andData: (NSString *) data {
    const char *cKey  = [key cStringUsingEncoding:NSASCIIStringEncoding];
    const char *cData = [data cStringUsingEncoding:NSUTF8StringEncoding];
    unsigned char cHMAC[CC_SHA512_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA512, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
    
    NSData *HMAC = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];
    NSString *hash = [HMAC base64Encoding];
    return hash;
//    NSData *HMACData = [NSData dataWithBytes:cHMAC length:sizeof(cHMAC)];
//    const unsigned char *buffer = (const unsigned char *)[HMACData bytes];
//    NSMutableString *HMAC = [NSMutableString stringWithCapacity:HMACData.length * 2];
//
//    for (int i = 0; i < HMACData.length; ++i){
//        [HMAC appendFormat:@"%02hhx", buffer[i]];
//    }
//
//    return HMAC;
}

@end
