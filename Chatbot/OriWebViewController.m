//
//  OriWebViewController.m
//  OriObjective
//


#import "OriWebViewController.h"
#import "Reachability.h"
#import "OriConstants.h"
#include <CommonCrypto/CommonDigest.h>
#import "HMAC_SHA512.h"
#import "UIWebView+TS_JavaScriptContext.h"

@interface OriWebViewController () <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *alertLabel;
@property (weak, nonatomic) IBOutlet UIButton *retryButton;
@property (weak, nonatomic) IBOutlet UIWebView *webView;

- (IBAction)retryButtonTapped:(id)sender;

@end

@implementation OriWebViewController {
    NSString *phoneNumber;
    NSString *currentcircle;
    NSString *currentsegment;
    NSString *currentappVersion;
    NSString *urlString;
    NSString *replaceJsonString;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UINavigationBar* navbar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    UINavigationItem* navItem = [[UINavigationItem alloc] initWithTitle:@""];
    UIBarButtonItem* cancelBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(onTapCancel:)];
    navItem.leftBarButtonItem = cancelBtn;
    [navbar setItems:@[navItem]];
    [self.view addSubview:navbar];
    [self openWebView];
    // Do any additional setup after loading the view from its nib.
}


//-(void)onTapCancel:(UIBarButtonItem*)item{
//    NSString *closeString = [NSString stringWithFormat:@"window.androidObj.updateFromAndroid('endchat','');"];
//    [_webView stringByEvaluatingJavaScriptFromString:closeString];
//}

-(void)onTapCancel:(UIBarButtonItem*)item{    NSString *closeString = [NSString stringWithFormat:@"window.androidObj.updateFromAndroid('endchat','');"];     if(_webView.canGoBack) {        [_webView goBack];     }else{         [_webView stringByEvaluatingJavaScriptFromString:closeString];    }}

- (void)initialise {
    phoneNumber = @"";
    currentcircle = @"";
    currentsegment = @"";
    currentappVersion = @"";
}

-(void) openWebView {
    _alertLabel.hidden = true;
    _webView.hidden = false;
    _retryButton.hidden = true;
    _webView.delegate = self;
    
    if ([self connected]) {
        NSURL *url = [NSURL URLWithString:urlString];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        NSData *cookieData = [[NSUserDefaults standardUserDefaults] objectForKey:@"ApplicationCookie"];
        if ([cookieData length] > 0) {
            NSArray *cookies = [NSKeyedUnarchiver unarchiveObjectWithData:cookieData];
            for (NSHTTPCookie *cookie in cookies) {
                [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
            }
        }
        //        NSArray * all = [NSHTTPCookie cookiesWithResponseHeaderFields:[request allHTTPHeaderFields] forURL:url];
        //        [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookies:all forURL:url mainDocumentURL:nil];
        //
        //           // Now we can print all of the cookies we have:
        //           for (NSHTTPCookie *cookie in all)
        //               NSLog(@"Name: %@ : Value: %@, Expires: %@", cookie.name, cookie.value, cookie.expiresDate);
        //           NSArray * availableCookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:url];
        //           NSDictionary * headers = [NSHTTPCookie requestHeaderFieldsWithCookies:availableCookies];
        
        //        [request setAllHTTPHeaderFields:[NSHTTPCookie requestHeaderFieldsWithCookies:[NSHTTPCookieStorage sharedHTTPCookieStorage].cookies]];
        
        // we are just recycling the original request
        [request allHTTPHeaderFields];
        [request HTTPShouldHandleCookies];
        [_webView loadRequest:request];
    }
    else {
        [self showAlert];
    }
    
}

-(void) showAlert {
    _alertLabel.hidden = false;
    _webView.hidden = true;
    _retryButton.hidden = false;
    _alertLabel.text = @"It seems that you are not connected to the internet. Please connect and try again";
}

- (BOOL) connected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

- (NSString*) getUidForPhone:(NSString*)phoneNumber {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *value = [userDefaults valueForKey:phoneNumber];
    return value;
}

- (void) saveUIDForPhone: (NSString*)phoneNumber {
    if (![self checkIfUidExist:phoneNumber]) {
        NSString *uuId = [OriWebViewController uuid];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:uuId forKey:phoneNumber];
        [userDefaults synchronize];
    }
}

+ (NSString *)uuid {
    CFUUIDRef uuidRef = CFUUIDCreate(NULL);
    CFStringRef uuidStringRef = CFUUIDCreateString(NULL, uuidRef);
    CFRelease(uuidRef);
    return (__bridge NSString *)uuidStringRef;
}

- (BOOL) checkIfUidExist:(NSString*)phoneNumber {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults valueForKey:phoneNumber] != nil) {
        return true;
    }
    return false;
}

- (void) initViewWithUrl:(NSString*)url withPhoneNumber:(NSString*)phonenumber withCircle:(NSString*)circle withSegment:(NSString*)segment withAppVersion:(NSString*)appVersion {
    phoneNumber = phonenumber;
    urlString = url;
    currentcircle = circle;
    currentsegment = segment;
    currentappVersion = appVersion;
    [self saveUIDForPhone:phoneNumber];
}

-(NSDictionary*) createObj {
    NSDictionary *appDetails = [[NSDictionary alloc] initWithObjectsAndKeys:BRAND_NAME,@"brandName",phoneNumber,@"msisdn",currentcircle,@"circle",currentsegment,@"segment",currentappVersion,@"appVersion", nil];
    
    NSDictionary *rawData = [[NSDictionary alloc] initWithObjectsAndKeys:appDetails,@"AppDetails",phoneNumber,@"udf1", @"",@"udf2",@"",@"udf3",@"",@"udf4",@"",@"udf5",BRAND_NAME,@"brandName",@"true",@"la",TOKEN2,@"token", nil];
    
    replaceJsonString = [self convertJsonToString:rawData];
    replaceJsonString = [replaceJsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    replaceJsonString = [replaceJsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString *encryptedString = [HMAC_SHA512 hashedValue:TOKEN  andData:replaceJsonString];
    NSDictionary *lockedParams = [[NSDictionary alloc]initWithObjectsAndKeys:rawData,@"rawData", encryptedString,@"hash", nil];
    NSString *uuid = [self getUidForPhone:phoneNumber];
    if (uuid != nil) {
        NSDictionary *obj = [[NSDictionary alloc]initWithObjectsAndKeys:uuid,@"psid", lockedParams,@"params", nil];
        return obj;
    }
    
    return nil;
}

-(NSString*) convertJsonToString:(NSDictionary*) dict {
    NSError *error = nil;
    NSData *json;
    
    if ([NSJSONSerialization isValidJSONObject:dict])
    {
        // Serialize the dictionary
        json = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
        
        // If no errors, let's view the JSON
        if (json != nil && error == nil)
        {
            NSString *jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
            return jsonString;
        }
    }
    return nil;
}




- (IBAction)retryButtonTapped:(id)sender {
    [self openWebView];
}

-(void) webViewDidFinishLoad:(UIWebView *)webView {
    
    NSData *cookieData = [NSKeyedArchiver archivedDataWithRootObject:[[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]];
    [[NSUserDefaults standardUserDefaults] setObject:cookieData forKey:@"ApplicationCookie"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    JSContext *context =  [webView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"]; // Undocumented access
    context[@"updateFromWeb"] = ^(NSString *param1, NSDictionary *param2) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self handleWebViewParameters:param1 param2:param2];
        });
    };
    
    
    NSDictionary *obj = [self createObj];
    NSString *jsonString = [self convertJsonToString:obj];
    NSString *finalString = [self convertJsonString:jsonString replaceWith:replaceJsonString];
    finalString = [finalString stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"];
    
    if (finalString != nil) {
        NSString *firstInput = [NSString stringWithFormat:@"window.androidObj.updateFromAndroid('ios','');"];
        
        BOOL isSuccess = [webView stringByEvaluatingJavaScriptFromString:firstInput];
        
        NSString *secondInput = [NSString stringWithFormat:@"window.androidObj.updateFromAndroid('psid', '%@');",finalString];
        
        isSuccess = [webView stringByEvaluatingJavaScriptFromString:secondInput];
    }
}

- (void)handleWebViewParameters:(NSString *)param1 param2:(NSDictionary*)dict {
    if ([param1 isEqualToString:@"endChatSubmit"]) {
        [self closeWebView];
    }
    NSLog(@"User clicked submit. param1=%@ and param2=%@", param1,dict);
}

-(void) closeWebView {
    [self dismissViewControllerAnimated:true completion:nil];
}

-(NSString*) convertJsonString:(NSString*)oldString replaceWith:(NSString*)tempString {
    NSString *resultString = @"";
    NSArray *resultArray = [oldString componentsSeparatedByString:@"\"rawData\" :"];
    resultString = [NSString stringWithFormat:@"%@%@", resultString,resultArray[0]];
    resultString = [NSString stringWithFormat:@"%@%@%@", resultString,@"\"rawData\" :",tempString];
    NSString *remainingString = resultArray[1];
    NSMutableArray *resultArray2 = [[remainingString componentsSeparatedByString:@"}"] mutableCopy];
    [resultArray2 removeObjectAtIndex:0];
    NSString *lastString = [resultArray2 componentsJoinedByString:@"}"];
    resultString = [NSString stringWithFormat:@"%@%@", resultString,lastString];
    return resultString;
    
}

@end


