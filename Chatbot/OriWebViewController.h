//
//  OriWebViewController.h
//  OriObjective
//
//  Created by Kumari Akanksha on 25/01/20.
//  Copyright © 2020 Kumari Akanksha. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OriWebViewController : UIViewController
- (void) initViewWithUrl:(NSString*)url withPhoneNumber:(NSString*)phonenumber withCircle:(NSString*)circle withSegment:(NSString*)segment withAppVersion:(NSString*)appVersion;
@end

NS_ASSUME_NONNULL_END
