//
//  WebManager.h
//  OriObjective
//
//  Created by Kumari Akanksha on 25/01/20.
//  Copyright © 2020 Kumari Akanksha. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OriWebViewController.h"
//#import "CustomNavigationController.h"
NS_ASSUME_NONNULL_BEGIN

@interface WebManager : NSObject

+(UINavigationController*) openWebViewWithPhoneNumber:(NSString*)phoneNumber withCircle: (NSString*)circle withSegment: (NSString*)segment withAppVersion: (NSString*)appVersion; 

@end

NS_ASSUME_NONNULL_END
